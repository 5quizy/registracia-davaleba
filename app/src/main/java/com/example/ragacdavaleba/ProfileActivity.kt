package com.example.ragacdavaleba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class ProfileActivity : AppCompatActivity() {

    private lateinit var GliciniBotton : Button
    private lateinit var LogOutBotton : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()

        clickListeners()

        logoutListeners()
    }


    private fun init(){
        GliciniBotton = findViewById(R.id.GliciniBotton)
        LogOutBotton = findViewById(R.id.LogOutBotton)

    }
    private fun clickListeners(){
        GliciniBotton.setOnClickListener(){
            Toast.makeText(this, "KLOUNADA", Toast.LENGTH_SHORT).show()
        }

    }

    private fun logoutListeners(){
        LogOutBotton.setOnClickListener(){

            startActivity(Intent(this, MainActivity::class.java))
        }


    }
}