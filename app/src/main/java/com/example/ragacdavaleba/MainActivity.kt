package com.example.ragacdavaleba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    private lateinit var editTextEmail : EditText
    private lateinit var editTextPassword : EditText
    private lateinit var editTextConfirmPassword : EditText
    private lateinit var submitButton : Button


    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        clickListeners()
    }

    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword)
        submitButton = findViewById(R.id.submitButton)


    }

    private fun clickListeners(){
        submitButton.setOnClickListener(){
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val confirmPassword = editTextPassword.text.toString()
            if (!email.contains("@")){
                Toast.makeText(this, "Please type correct email", Toast.LENGTH_SHORT).show()
            }
            if(password.toString().length < 9){
                Toast.makeText(this, "password length should be 9 symbols or more", Toast.LENGTH_SHORT).show()
            }
            if (password != confirmPassword){
                Toast.makeText(this, "confirm your password correctly", Toast.LENGTH_SHORT).show()
            }else{
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(){ task ->
                        if (task.isSuccessful){
                            Toast.makeText(this, "You registered successfully", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this, ProfileActivity::class.java))
                            finish()
                        }

                    }

            }

        }

    }


}